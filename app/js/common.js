$(function() {
	// Params
	var sliderSelector = '.swiper-container',
		options = {
			init: false,
			loop: true,
			speed:800,
			slidesPerView: 3, // or 'auto'
			// spaceBetween: 10,
			centeredSlides : true,
			effect: 'coverflow', // 'cube', 'fade', 'coverflow',
			coverflowEffect: {
				rotate: 0, // Slide rotate in degrees
				stretch: 100, // Stretch space between slides (in px)
				depth: 150, // Depth offset in px (slides translate in Z axis)
				modifier: 1, // Effect multipler
				slideShadows : true, // Enables slides shadows
			},
			grabCursor: true,
			parallax: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				1023: {
					slidesPerView: 2,
					spaceBetween: 0
				}
			},
			// Events
			on: {
				imagesReady: function(){
					this.el.classList.remove('loading');
				}
			}
		};
	var mySwiper = new Swiper(sliderSelector, options);


// Initialize slider
	mySwiper.init();

	var galleryThumbs = new Swiper('.gallery-thumbs', {
		spaceBetween: 10,
		centeredSlides: true,
		slidesPerView: 1,
		touchRatio: 0.2,
		slideToClickedSlide: true,
		loop: true,
		loopedSlides: 5
	});
	mySwiper.controller.control = galleryThumbs;
	galleryThumbs.controller.control = mySwiper;


	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "/mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Спасибо!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
});

